use super::comments::{CodeComments, CommentType};

use colored::*;

pub fn terminal_output(dir: &str, code_comments: CodeComments, is_tags_only: bool) {
    println!("\nSearching in {} ...\n", dir.cyan().bold());

    if code_comments.get_file_paths().is_empty() {
        println!("No comments to be found ...");
        return;
    }

    for file_path in code_comments.get_file_paths() {
        if !code_comments
            .get_comments(file_path, is_tags_only)
            .is_empty()
        {
            println!(
                "{} {} {}\n",
                "in".green().bold(),
                file_path.green().bold(),
                "...".green().bold()
            );

            print_comments(&code_comments, file_path, is_tags_only);

            println!("\n{:->80}\n", " ");
        }
    }
}

fn print_comments(code_comments: &CodeComments, file_path: &String, is_tags_only: bool) {
    for comment in code_comments.get_comments(file_path, is_tags_only) {
        let comment_text = maybe_trim_comment_text(comment.get_text().to_string());
        let comment_line_number = comment.get_line_num();
        match comment.get_comment_type() {
            CommentType::ToDo => {
                println!(
                    "\n{}{}",
                    "  -- TODO item --".bright_yellow().bold(),
                    "-".repeat(23).bright_yellow().bold()
                );
                println!(
                    "  {} {:<3} : {}",
                    "line".purple().italic(),
                    comment_line_number.to_string().purple().italic(),
                    comment_text
                );
                println!("  {}\n", "-".repeat(38).bright_yellow().bold());
            }
            CommentType::Bug => {
                println!(
                    "\n{}{}",
                    "  -- BUG item --".bright_red().bold(),
                    "-".repeat(24).bright_red().bold()
                );
                println!(
                    "  {} {:<3} : {}",
                    "line".purple().italic(),
                    comment_line_number.to_string().purple().italic(),
                    comment_text
                );
                println!("  {}\n", "-".repeat(38).bright_red().bold());
            }
            CommentType::Text => {
                println!(
                    "  {} {:<3} : {}",
                    "line".purple().italic(),
                    comment_line_number.to_string().purple().italic(),
                    comment_text
                )
            }
        }
    }
}

fn maybe_trim_comment_text(comment_text: String) -> String {
    if comment_text.len() < 80 {
        return comment_text;
    }

    let mut new_text = comment_text;
    new_text.replace_range(67..new_text.len(), "");
    new_text.replace_range(
        new_text.rfind(' ').unwrap_or(new_text.len())..new_text.len(),
        " ... ",
    );
    String::from(&new_text)
}
