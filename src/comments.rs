use super::languages;

use std::{
    collections::HashMap,
    ffi::OsStr,
    fs::File,
    io::{BufRead, BufReader},
};
use walkdir::DirEntry;

#[derive(Debug, Clone)]
pub enum CommentType {
    Text,
    ToDo,
    Bug,
}

#[derive(Debug, Clone)]
pub struct CodeComment {
    comment_text: String,
    comment_type: CommentType,
    line_num: i32,
}

impl CodeComment {
    pub fn get_text(&self) -> &String {
        &self.comment_text
    }

    pub fn get_comment_type(&self) -> &CommentType {
        &self.comment_type
    }

    pub fn get_line_num(&self) -> i32 {
        self.line_num
    }
}

#[derive(Debug, Clone)]
pub struct CodeComments {
    comments: HashMap<String, Vec<CodeComment>>,
}

impl CodeComments {
    pub fn new() -> CodeComments {
        CodeComments {
            comments: HashMap::new(),
        }
    }

    pub fn get_file_paths(&self) -> Vec<&String> {
        self.comments.keys().into_iter().collect()
    }

    pub fn get_comments(&self, file_path: &String, is_tags_only: bool) -> Vec<CodeComment> {
        if is_tags_only {
            let mut comments: Vec<CodeComment> = Vec::new();
            for comment in self.comments.get(file_path).expect("key not found") {
                match comment.get_comment_type() {
                    CommentType::Text => {}
                    _ => comments.push(comment.to_owned()),
                }
            }
            return comments;
        }

        self.comments
            .get(file_path)
            .expect("key not found")
            .to_owned()
    }

    pub fn insert_comment(
        &mut self,
        path: &str,
        comment_text: String,
        comment_type: CommentType,
        line_num: i32,
    ) {
        self.comments
            .entry(path.to_string())
            .or_insert(Vec::new())
            .push(CodeComment {
                comment_text,
                comment_type,
                line_num,
            });
    }
}

pub fn collect_file_comments(dir: &str, code_file_paths: Vec<DirEntry>) -> CodeComments {
    let mut comments = CodeComments::new();

    for code_file in code_file_paths {
        let comment_starter = get_comment_starter(&code_file);

        let mut line_counter = 1;
        for file_line in BufReader::new(File::open(&code_file.path()).unwrap())
            .lines()
            .flatten()
        {
            if file_line.trim().starts_with(&comment_starter) {
                let mut comment_type = CommentType::Text;
                if file_line.starts_with("// FIXME ") || file_line.contains("// BUG ") {
                    comment_type = CommentType::Bug;
                } else if file_line.starts_with("// TODO ") {
                    comment_type = CommentType::ToDo;
                }

                comments.insert_comment(
                    &code_file.path().to_str().unwrap().replace(dir, ""),
                    file_line.trim().to_string(),
                    comment_type,
                    line_counter,
                );
            }

            line_counter += 1;
        }
    }

    comments
}

fn get_comment_starter(entry: &DirEntry) -> String {
    let assoc = languages::LanguageAssociations::new();

    assoc
        .get_lanugage_association(entry.path().extension().and_then(OsStr::to_str).unwrap())
        .unwrap()
        .to_string()
}
