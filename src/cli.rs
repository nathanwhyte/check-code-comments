use clap::Parser;
use std::env;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// Path to the directory to search in (defaults to cwd)
    directory: Option<String>,

    /// Search in specified directory only
    #[clap(short, long)]
    shallow: bool,

    /// Show only comments with TODO, BUG, or FIXME tags
    #[clap(short, long)]
    tags: bool,
}

fn get_cwd() -> String {
    String::from(
        env::current_dir()
            .expect("Error loading current working directory!")
            .to_str()
            .expect("Error loading current working directory!"),
    )
}

fn get_parent_dir() -> String {
    String::from(
        env::current_dir()
            .unwrap()
            .ancestors()
            .nth(1)
            .unwrap()
            .to_str()
            .unwrap(),
    )
}

impl Args {
    pub fn new() -> Args {
        Args::parse()
    }

    pub fn get_directory(&self) -> String {
        let dir: &str = match &self.directory {
            Some(spec_dir) => spec_dir,
            None => ".",
        };

        match dir {
            "." => get_cwd(),
            ".." => get_parent_dir(),
            _ => String::from(dir)
        }
    }

    pub fn is_shallow(&self) -> bool {
        self.shallow
    }

    pub fn is_tags_only(&self) -> bool {
        self.tags
    }
}
