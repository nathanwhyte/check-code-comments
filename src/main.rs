mod cli;
mod comments;
mod files;
mod languages;
mod terminal;

fn main() {
    let args = cli::Args::new();

    let files = match args.is_shallow() {
        false => files::get_files_recursive(&args.get_directory()),
        true => files::get_files(&args.get_directory()),
    };

    let code_comments = comments::collect_file_comments(&args.get_directory(), files);
    terminal::terminal_output(&args.get_directory(), code_comments, args.is_tags_only());
}
