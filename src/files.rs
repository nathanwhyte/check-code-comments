use super::languages;

use walkdir::{DirEntry, WalkDir};

pub fn get_files(dir: &str) -> Vec<DirEntry> {
    let assoc = languages::LanguageAssociations::new();
    let code_file_extensions = assoc.get_code_file_extensions();

    WalkDir::new(dir)
        .max_depth(1)
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
        .filter(|e| {
            e.file_name()
                .to_str()
                .map(|f| !f.starts_with('.'))
                .unwrap_or(false)
        })
        .filter(|e| match e.path().extension() {
            Some(path_os_str) => code_file_extensions.contains(&path_os_str.to_str().unwrap()),
            None => false,
        })
        .collect()
}

pub fn get_files_recursive(dir: &str) -> Vec<DirEntry> {
    let assoc = languages::LanguageAssociations::new();
    let code_file_extensions = assoc.get_code_file_extensions();

    WalkDir::new(dir)
        .into_iter()
        .filter_map(Result::ok)
        .filter(|e| e.file_type().is_file())
        .filter(|e| {
            e.file_name()
                .to_str()
                .map(|f| !f.starts_with('.'))
                .unwrap_or(false)
        })
        .filter(|e| match e.path().extension() {
            Some(path_os_str) => code_file_extensions.contains(&path_os_str.to_str().unwrap()),
            None => false,
        })
        .collect()
}
