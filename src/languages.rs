use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct LanguageAssociations<'a> {
    associations: HashMap<&'a str, &'a str>,
}

impl LanguageAssociations<'_> {
    pub fn new() -> LanguageAssociations<'static> {
        LanguageAssociations {
            associations: HashMap::from([
                ("rs", "// "),
                ("py", "# "),
                ("java", "// "),
                ("sh", "# "),
                ("js", "// "),
            ]),
        }
    }

    pub fn get_lanugage_association(&self, language: &str) -> Option<&str> {
        if let Some(association) = self.associations.get(language) {
            Some(association)
        } else {
            None
        }
    }

    pub fn get_code_file_extensions(&self) -> Vec<&str> {
        let code_file_assoc = self.associations.clone();
        code_file_assoc.into_keys().collect()
    }
}
